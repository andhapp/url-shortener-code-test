Rails.application.routes.draw do
  resources :short_urls, only: [:create, :show], path: '/'
end
