require 'rails_helper'

RSpec.describe 'Url shortening', :type => :request do
  let(:original_url) { 'http://test.com' }

  describe 'POST /' do
    context 'when url is present in request params' do
      it 'returns a JSON object of original url and shortened path' do
        post '/', params: { url: original_url }

        aggregate_failures do
          response_body = JSON.parse(response.body)

          expect(response.status).to eq(201)
          expect(response_body['url']).to eq original_url
          expect(response_body['short_url']).not_to be_empty
        end
      end
    end

    context 'when url is missing in request params' do
      it 'returns an error JSON object' do
        post '/', params: {}

        aggregate_failures do
          expect(response.status).to eq(400)
          expect(JSON.parse(response.body)).
           to eq({ 'error'=> {'title' => 'url is required' } })
        end
      end
    end
  end

  describe 'GET /' do
    context 'when the shortened_url is present' do
      it 'redirects to the original url for the shortened_url' do
        post '/', params: { url: original_url }

        shortened_url = JSON.parse(response.body)['short_url']

        get "/#{shortened_url}"

        expect(response).to redirect_to(original_url)
      end

      context 'when the original url is missing the protocol' do
        let(:original_url) { 'example.com' }

        it 'redirects to the original url (with http prepended) for the shortened url' do
          post '/', params: { url: original_url }

          shortened_url = JSON.parse(response.body)['short_url']

          get "/#{shortened_url}"

          expect(response).to redirect_to("http://#{original_url}")
        end
      end
    end

    context 'when the shortened_url is not present' do
      it 'returns a 404' do
        get '/never-been-shortened'

        expect(response.status).to eq(404)
      end
    end
  end
end
