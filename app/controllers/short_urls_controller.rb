class ShortUrlsController < ApplicationController
  def create
    original_url = params[:url]

    if original_url.blank?
      render json: { error: { title: 'url is required' } }, status: 400
      return
    end

    short_url = ShortUrl.new(original_url)
    short_url.save

    render json: short_url.as_json, status: 201
  end

  def show
    original_url = ShortUrl.find(shortened_url: params[:id])

    unless original_url
      head(404)
      return
    end

    unless URI.parse(original_url).is_a?(URI::HTTP)
      original_url.prepend('http://')
    end

    redirect_to original_url, status: 301
  end
end
