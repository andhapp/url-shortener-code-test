class ShortUrl
  attr_accessor :original_url, :shortened_url

  def self.find(shortened_url:)
    read_from_memory(shortened_url)
  end

  def self.read_from_memory(key)
    Rails.cache.fetch("/#{key}")
  end

  def initialize(original_url)
    @original_url = original_url
  end

  def save
    @shortened_url = "/#{SecureRandom.hex(5)}"
    write_in_memory(shortened_url, original_url)
  end

  def as_json
    { url: original_url, short_url: shortened_url }
  end

  private

  def write_in_memory(key, value)
    Rails.cache.write(key, value)
  end
end
